#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ehrt/rt.h>
#include <ehrt/value.h>
#include <ehrt/panic.h>

#define error(x, ...) { \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
        exit(x); \
    }

#define STACK_SIZE (32*1024*1024) /* 32MiB */
#define FRAMES_SIZE (32*1024*1024) /* 32MiB */
#define HEAP_SIZE (128*1024*1024) /* 128MiB */
#define CALL_SIZE (1024*1024) /* 1MiB */

EhValue *__main_init__(EhRt *rt, EhValue **data, EhValue **argv);

void __ehprog_start(EhRt *rt, void *data) {
    EhValue *function = new_function(rt, NULL, __main_init__, (EhHtrie *) ehrt_peek(rt, 0), 0, NULL, 0, NULL);
    EhValue *closure = new_closure(rt, NULL, 0, 1, (EhFunction **) &function);
    ehrt_push(rt, closure);
    jmp_buf jmp;
    if (!setjmp(jmp)) {
        ehrt_pushcall(rt, (EhClosure *) closure, __main_init__, NULL, 0, &jmp);
        __main_init__(rt, NULL, NULL);
    }
}

int main(int argc, char **argv) {
    EhInfo info;
    info.thread_id = 0;
    info.stack = STACK_SIZE;
    info.frames = FRAMES_SIZE;
    info.heap = HEAP_SIZE;
    info.call_stack = CALL_SIZE;
    info.data = NULL;

    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            if (!strcmp(argv[i], "--")) {
                ++i;
                info.argc = argc - i + 1;
                info.argv = malloc(info.argc * sizeof(char *));
                info.argv[0] = argv[0];
                for (int j = 1; j < info.argc; j++) {
                    info.argv[j] = argv[i + j - 1];
                }
                break;
            } else if (!strcmp(argv[i], "--heap") || !strcmp(argv[i], "-H")) {
                ++i;
                char *end;
                info.heap = strtoul(argv[i], &end, 10);
                if (!*argv[i] || *end) {
                    printf("%d\n", *argv[i]);
                    printf("%d\n", *end);
                    error(1, "--heap must be an unsized integer: %s", argv[i]);
                }
            } else if (!strcmp(argv[i], "--stack") || !strcmp(argv[i], "-S")) {
                ++i;
                char *end;
                info.stack = strtoul(argv[i], &end, 10);
                if (!*argv[i] || *end) {
                    error(1, "--stack must be an unsized integer: %s", argv[i]);
                }
            } else if (!strcmp(argv[i], "--frames") || !strcmp(argv[i], "-F")) {
                ++i;
                char *end;
                info.frames = strtoul(argv[i], &end, 10);
                if (!*argv[i] || *end) {
                    error(1, "--frames must be an unsized integer: %s", argv[i]);
                }
            } else if (!strcmp(argv[i], "--call-stack") || !strcmp(argv[i], "-C")) {
                ++i;
                char *end;
                info.call_stack = strtoul(argv[i], &end, 10);
                if (!*argv[i] || *end) {
                    error(1, "--call-stack must be an unsized integer: %s", argv[i]);
                }
            } else {
                info.argc = argc - i + 1;
                info.argv = malloc(info.argc * sizeof(char *));
                info.argv[0] = argv[0];
                for (int j = 1; j < info.argc; j++) {
                    info.argv[j] = argv[i + j - 1];
                }
                break;
            }
        }
    } else {
        info.argc = 1;
        info.argv = malloc(info.argc * sizeof(char *));
        info.argv[0] = argv[0];
    }

    ehrt_start(__ehprog_start, &info);
}
